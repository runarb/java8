package no.jpro.java8;

import com.google.common.collect.Lists;

import java.math.BigDecimal;
import java.util.Comparator;
import java.util.List;

import static java.util.Comparator.comparing;

public class V1_SortingLambdasMethodpassing {
    public static void main(String[] args) {
        V1_SortingLambdasMethodpassing v1_sortingLambdasMethodpassing = new V1_SortingLambdasMethodpassing();
        v1_sortingLambdasMethodpassing.sortV8Lambdas();
    }


    List<Person> people = Lists.newArrayList(
            new Person("Ole", "75.0", 38),
            new Person("Nils", "82.3", 47),
            new Person("Fredrik", "72.3", 43),
            new Person("Tina", "62.7", 41)
    );

    public void sortPreV8() {
        people.sort(new Comparator<Person>() {
            @Override
            public int compare(Person o1, Person o2) {
                return o1.getAge() - o2.getAge();
            }
        });
        System.out.println("sorted by age:    " + people);

        people.sort(new Comparator<Person>() {
            @Override
            public int compare(Person o1, Person o2) {
                return o1.getName().compareTo(o2.getName());
            }
        });
        System.out.println("sorted by name:   " + people);

        people.sort(new Comparator<Person>() {
            @Override
            public int compare(Person o1, Person o2) {
                return o1.getWeight().compareTo(o2.getWeight());
            }
        });
        System.out.println("sorted by weight: " + people);
    }

    public void sortV8Lambdas() {
        people.sort((p1, p2) -> p1.getName().compareTo(p2.getName()));
        people.sort((p1, p2) -> p1.getAge() - p2.getAge());

        System.out.println(people);
    }

    public void sortV8MethodPassing() {
        people.sort(comparing(Person::getAge));
    }


    public static class Person {
        final public String name;
        final public BigDecimal weight;
        final public int age;

        public Person(String name, BigDecimal weight, int age) {
            this.name = name;
            this.weight = weight;
            this.age = age;
        }
        public Person(String name, String weight, int age) {
            this.name = name;
            this.weight = new BigDecimal(weight);
            this.age = age;
        }

        public String getName() {
            return name;
        }

        public BigDecimal getWeight() {
            return weight;
        }

        public int getAge() {
            return age;
        }

        @Override
        public String toString() {
            return "Person{" +
                    "name='" + name + '\'' +
                    ", weight=" + weight +
                    ", age=" + age +
                    '}';
        }
    }
}

