package no.jpro.java8;

import java.math.BigDecimal;
import java.util.function.Function;

import static java.lang.System.out;
public class LambdaTest {
    public static void main(String[] args) {
//        ScopeExample.doStuff();
//        new ScopeExample().effectivelyFinal();
        new ScopeExample().functions();
//        new ScopeExample().functionMagicHappens();
    }


    static class ScopeExample {
        Runnable r1 = () -> out.println(this);
        Runnable r2 = () -> out.println(toString());

        public String toString() { return "Hello, world!"; }

        public static void doStuff() {
            new ScopeExample().r1.run();
            new ScopeExample().r2.run();
        }

        public void effectivelyFinal() {

            String str = "foobar";
            Thread t1 = new Thread(() -> System.out.println(str));
//            str = "james";
            t1.run();

        }

        public void functions() {
            Function<String, String> atr = (name) -> {return "#" + name;};
            Function<String, Integer> length = String::length;
            Function<Integer, String> intToString = (integer) -> {return "foobar" + integer;};

            System.out.println(length.compose(atr).andThen(intToString).apply("seven"));
        }

        public void functionMagicHappens() {
            DoMagic<String, BigDecimal> bd = (number) -> { return new BigDecimal(number); };

            System.out.println(bd.magicHappens("2013.14").abs());
        }
    }



}
