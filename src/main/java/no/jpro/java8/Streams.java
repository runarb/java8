package no.jpro.java8;

import com.google.common.collect.Lists;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.ForkJoinPool;
import java.util.stream.Stream;

public class Streams {

    public static void main(String[] args) {
        Streams streams = new Streams();
        streams.evaluateHighestPoints();
    }

    public void streamTests() {
        Stream<Integer> s = Stream.of(1, 2, 3);
        Stream<String> s2 = Arrays.stream(new String[]{"a", "b", "c"});

    }

    public static class PlayerPoints {
        public final String name;
        public final long points;

        public PlayerPoints(String name, long points) {
            this.name = name;
            this.points = points;
        }

        public String toString() {
            return name + ":" + points;
        }
    }

    public static long getPoints(final String name) {
        long round = Math.round(Math.random() * 100);
        System.out.println(name + " got " + round + " points");
        return round;
    }

    public static void evaluateHighestPoints() {
        List<String> names = Arrays.asList("Foo", "Bar", "Baz");
        PlayerPoints highestPlayer =
                names.stream().map(name -> new PlayerPoints(name, getPoints(name)))
                .reduce(new PlayerPoints("", 0),
                        (s1, s2) -> (s1.points > s2.points) ? s1 : s2);

        System.out.println("Highest scorer = " + highestPlayer);
    }


    public void peek() throws IOException {
        Files.list(Paths.get("."))
             .map(Path::getFileName)
             .peek(System.out::println)
             .forEach(p -> doSomething(p));
    }

    private void doSomething(Path p) {
        System.out.println(p.toString());
    }
}
