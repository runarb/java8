package no.jpro.java8;

import java.math.BigInteger;
import java.util.function.Function;

public class Euler20 {
    private Function<Long, BigInteger> factorial;

    public static void main(String[] args) {
        Euler20 e20 = new Euler20();
        BigInteger num = e20.doFactorial(100L);
        String s = num.toString();
        long sum = 0;
        for (int i = 0; i < s.length(); i++) {
            sum += new Long(s.substring(i, i+1));
        }
        System.out.println("sum = " + sum);
    }

    public BigInteger doFactorial(long number) {
        factorial = (n) -> (n <= 0L) ? new BigInteger("1") : factorial.apply(n - 1L).multiply(new BigInteger("" + n));

        return factorial.apply(number);
    }


}
