package no.jpro.java8;

@FunctionalInterface
public interface DoMagic<T, R> {
    public R magicHappens(T whatever);
    default void notMagic() {
        System.out.println("Inne i defaultmetoden notMagic");
    }
}

