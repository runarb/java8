package no.jpro.java8;

import java.io.File;
import java.util.Comparator;
import java.util.stream.Stream;

public class Filters {
    public static class FileFilters {
        public static boolean fileIsPdf(File file) {return true;};
        public static boolean fileIsTxt(File file) {return true;};
        public static boolean fileIsDoc(File file) {return true;};
    }

    public static void main(String[] args) {
        Stream<File> pdfs = getFiles().filter(FileFilters::fileIsPdf);
        Stream<File> txts = getFiles().filter(FileFilters::fileIsTxt);
        Stream<File> docs = getFiles().filter(FileFilters::fileIsDoc);

        pdfs.sorted(Comparator.comparing(File::getParent).thenComparing(File::getName));
    }

    public static Stream<File> getFiles() {
        return null;
    }
}
