package no.jpro.java8;

public class DefaultMethods {

    public static void main(String[] args) {
        FooBar fooBar = new FooBar();
        fooBar.talk();
    }

    interface Foo {
        default void talk() {
            System.out.println("Foo!");
        }
    }
    interface Bar {
        default void talk() {
            System.out.println("Bar!");
        }
    }
    static class FooBar implements Foo, Bar {
        @Override
        public void talk() { Foo.super.talk(); Bar.super.talk(); }
    }
}

